package it.jac.gallery.entity;

import java.util.List;

import lombok.Data;

@Data
public class Gallery {
	private Integer id;
	private List<Object> images;
}
